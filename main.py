#!/usr/bin/env python3
import busio
import threading
import digitalio
import board
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn
import time 
import digitalio
import RPi.GPIO as GPIO
import os

#current sampling rate 
rate = 0  

#starting time 
start_time = 0  

#button pin
btn = 16       

#sampling rate
samplingRate = {0:1, 1:5, 2:10} 

#LDR and Sensor Chanels
ldr_chan = None                 
temp_chan = None                 

runtime = 'Runtime'
read = "Reading"
temp = "Temp"
ldr_reading = "LDR Reading"
ldr_voltage = "LDR Voltage"

def setup():

    global ldr_chan
    global temp_chan
    
    
    spi = busio.SPI( clock = board.SCK, MISO = board.MISO, MOSI = board.MOSI )
    cs = digitalio.DigitalInOut(board.D5)
    mcp = MCP.MCP3008( spi, cs )
    ldr_chan = AnalogIn( mcp, MCP.P2)
    temp_chan = AnalogIn( mcp, MCP.P1)
    GPIO.setup(btn, GPIO.IN)
    GPIO.setup(btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(btn, GPIO.FALLING, callback=btn_pressed, bouncetime=200)


def sensor_thread():
    
    thread = threading.Timer(samplingRate[rate], sensor_thread) #this thread runs after
    thread.daemon = True
    thread.start()

    
    current_time = int( time.time() - start_time )

    
    str_temp = str( round( (temp_chan.voltage - 0.5)/0.01, 2 ) ) + " C"
    str_tempVal = temp_chan.value
    str_runtime = str(current_time) + "s"
    str_ldrValue = ldr_chan.value
    str_ldrVoltage = str( round( ldr_chan.voltage, 2 )) + " V"

    print("{:<15}{:<15}{:<15}{:<15}{:<15}".format( str_runtime, str_tempVal, str_temp, str_ldrValue, str_ldrVoltage))


def btn_pressed(channel):
    global rate
    rate = (rate + 1) % 3
    
    print("Sampling at : " + str(samplingRate[rate])+"s")
    print("{:<15}{:<15}{:<15}{:<15}{:<15}".format( runtime, read, temp, ldr_reading, ldr_voltage))



if __name__ == "__main__":
    try:
        setup()
        
        start_time = time.time() # define start time
        print("Sampling at : " + str(samplingRate[rate])+"s")
        print("{:<15}{:<15}{:<15}{:<15}{:<15}".format( runtime, read, temp, ldr_reading, ldr_voltage))

        sensor_thread()
        while True:
            pass
    except KeyboardInterrupt:
        print("\nExiting program.....")
    except Exception as e:
        print(e)
    finally:
        GPIO.cleanup()


